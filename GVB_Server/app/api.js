const express = require('express')

const api = express.Router()

const db = require('./db/db')
const dbHelper = require('./db/db-helper')
const dbConfig = require('./db/db-config')

/* Common handler function */
const handler = (req, res, next, table, fields, SQLBuilder) => {
  let sql
  try {
    sql = SQLBuilder(table, fields, req)
  }
  catch (err) {
    err.status = 400
    next(err)
    return
  }

  console.log('SQL:', sql)

  db.execute(sql, (err, result) => {
    if (err) {
      err.status = 500
      next(err)
      return
    }

    const json = dbHelper.buildJSON(fields, result)
    console.log('Number of rows:', json.length)
    console.log(json.length > 0 ? json[0] : '')

    res.json(json)
  })
}

/* Get records */
api.get('/records', (req, res, next) => {
  let fields = ['ID',
                  'Filename', 'FileDateTime',
                  'Read',
                  'Voice', 'VoiceScore', 'VoiceHumanFlag',
                  'Gender', 'GenderScore', 'GenderHumanFlag']

  for (let field of ['Language', 'Speaker', 'Keyword']) {
    if (req.query.hasOwnProperty(field)) {
      fields.push(field)
      fields.push(field + 'Score')
    }
  }

  fields.push('rowNum')

  handler(req, res, next, dbConfig.tableRecords, fields, dbHelper.buildSQL_Records)
})

/* Modify one record */
api.put('/record/:id', (req, res, next) => {
  handler(req, res, next, dbConfig.tableRecords, undefined, dbHelper.buildSQL_ModifyRecord)
})

/* Get continuous recognition result of one record */
api.get('/record/:id/Continuous', (req, res, next) => {
  const fields = ['InfoID', 'Speech', 'SpeechStart', 'SpeechEnd', 'SpeechHumanResult', 'Channel']

  handler(req, res, next, dbConfig.tableCON, fields, dbHelper.buildSQL_RecordContinous)
})

/* Modify continuous recognition result of one record */
api.put('/record/:id/Continuous', (req, res, next) => {
  handler(req, res, next, dbConfig.tableCON, undefined, dbHelper.buildSQL_ModifyContinuous)
})

/* Get info of one record */
api.get('/record/:id/:infoType', (req, res, next) => {
  let table
  let fields

  switch (req.params.infoType) {
    case 'Language':
      table = dbConfig.tableLID
      fields = ['InfoID', 'Language', 'LanguageScore', 'LanguageHumanFlag', 'LanguageHumanResult', 'Channel']
      break
    case 'Speaker':
      table = dbConfig.tableSID
      fields = ['InfoID', 'Speaker', 'SpeakerScore', 'SpeakerHumanFlag', 'Channel']
      break
    case 'Keyword':
      table = dbConfig.tableKW
      fields = ['InfoID', 'Keyword', 'KeywordStart', 'KeywordEnd', 'KeywordScore', 'KeywordHumanFlag', 'Channel']
      break
    default:
      let err = new Error('Illegal infoType.')
      err.status = 400
      next(err)
      return
      break
  }

  handler(req, res, next, table, fields, dbHelper.buildSQL_RecordInfo)
})

/* Modify language of one record */
api.put('/record/:id/Language', (req, res, next) => {
  handler(req, res, next, dbConfig.tableLID, undefined, dbHelper.buildSQL_ModifyLanguage)
})

/* Flag speaker of one record */
api.put('/record/:id/Speaker', (req, res, next) => {
  handler(req, res, next, dbConfig.tableSID, undefined, dbHelper.buildSQL_FlagSpeaker)
})

/* Flag keyword of one record */
api.put('/record/:id/Keyword', (req, res, next) => {
  handler(req, res, next, dbConfig.tableKW, undefined, dbHelper.buildSQL_FlagKeyword)
})

/* Add keyword */
api.post('/keyword', (req, res, next) => {
  handler(req, res, next, undefined, undefined, dbHelper.buildSQL_AddKeyword)
})

/* Add speaker */
api.post('/speaker', (req, res, next) => {
  handler(req, res, next, undefined, undefined, dbHelper.buildSQL_AddSpeaker)
})

/* Get audio server */
api.get('/audioserver', (req, res) => {
  res.send(process.env.AUDIO_SERVER || 'http://172.18.29.237:3030/')
})

module.exports = api
