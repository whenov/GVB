/* index */
create index IDX_VOICE_R_S on T_IV_VOICE_RECORD_SYSTEM(STARTTIME);
create index IDX_VOICE_R_READ_NID on T_IV_VOICE_RECORD_SYSTEM(READTAG,NID);

create index IDX_LID_FILEID on T_IV_ORI_LID_LIDINVOICE(FILEID);

create index IDX_SID_FILEID on T_IV_ORI_SID_SIDINVOICE(FILEID);

create index IDX_KW_FILEID on T_IV_ORI_KW_KWINVOICE(FILEID);

/* sequence and trigger */
create sequence SW_KWS_KID_SEQ;
create or replace trigger SW_KWS_KID_INC
before insert on T_IV_SW_KEYWORDS
for each row
begin
    select SW_KWS_KID_SEQ.nextval
    into :new.KID
    from dual;
end;

create sequence SW_KWR_KID_SEQ;
create or replace trigger SW_KWR_KID_INC
before insert on T_IV_SW_KEYWORDRECORD
for each row
begin
    select SW_KWR_KID_SEQ.nextval
    into :new.KID
    from dual;
end;

create sequence SID_TGS_KID_SEQ;
create or replace trigger SID_TGS_KID_INC
before insert on T_IV_SID_TARGETS
for each row
begin
    select SID_TGS_KID_SEQ.nextval
    into :new.KID
    from dual;
end;

create sequence SID_TGR_KID_SEQ;
create or replace trigger SID_TGR_KID_INC
before insert on T_IV_SID_TARGETRECORDS
for each row
begin
    select SID_TGR_KID_SEQ.nextval
    into :new.KID
    from dual;
end;

/* create table for continuous voice recognition */
create table T_IV_ORI_CON_CONINVOICE (
  "KID" number not null,
  "KWSTART" number(10,4) not null,
  "KWEND" number(10,4),
  "KWTEXT" varchar2(2048) not null,
  "KWHUMANRESULT" varchar2(2048),
  "FILEID" number(20,0) not null,
  "CHANNELSIDE" number(10,0) default 1
);

