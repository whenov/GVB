const dbConfig = require('./db-config.js')
const fieldMap = dbConfig.fieldMap

/* Construct a sql statement from a query for records */
exports.buildSQL_Records = (table, fields, req) => {
  let query = req.query

  let dbFields = []
  for (let field of fields) {
    let dbField
    if (field === 'rowNum') {
      continue
    } else if (field === 'Language' || field === 'LanguageScore') {
      dbField = 'l.' + fieldMap[field]
    } else if (field === 'Speaker' || field === 'SpeakerScore') {
      dbField = 's.' + fieldMap[field]
    } else if (field === 'Keyword' || field === 'KeywordScore') {
      dbField = 'k.' + fieldMap[field]
    } else {
      dbField = 'v.' + fieldMap[field]
    }
    dbFields.push(dbField)
  }
  dbFields = dbFields.join(', ')

  /* Conditions for BeginDateTime, EndDateTime, Read, Voice, VoiceScore, Gender, GenderScore */
  let condition = []

  if (query.hasOwnProperty('BeginDateTime')) {
    condition.push(`v.${fieldMap.BeginDateTime} >=
      to_date('${query.BeginDateTime}', 'YYYY-MM-DD\"T\"HH24:MI:SS')`)
    delete query.BeginDateTime
  }

  if (query.hasOwnProperty('EndDateTime')) {
    condition.push(`v.${fieldMap.EndDateTime} <
      to_date('${query.EndDateTime}', 'YYYY-MM-DD\"T\"HH24:MI:SS')`)
    delete query.EndDateTime
  }

  if (query.hasOwnProperty('Read')) { // need index on (READTAG, NID)
    condition.push(`v.${fieldMap.Read} = ${query.Read}`)
    delete query.Read
  }

  if (query.hasOwnProperty('Voice')) {
    condition.push(`v.${fieldMap.Voice} = ${query.Voice}`)
    delete query.Voice

    if (query.hasOwnProperty('VoiceScore')) {
      condition.push(`v.${fieldMap.VoiceScore} >= ${query.VoiceScore}`)
      delete query.VoiceScore
    }
  }

  if (query.hasOwnProperty('Gender')) {
    condition.push(`v.${fieldMap.Gender} = ${query.Gender}`)
    delete query.Gender

    if (query.hasOwnProperty('GenderScore')) {
      condition.push(`v.${fieldMap.GenderScore} >= ${query.GenderScore}`)
      delete query.GenderScore
    }
  }

  condition = condition.join(' and ')
  if (condition) {
    condition = 'where ' + condition
  }

  /* Conditions for Language, Speaker, Keyword, and their scores */
  let joins = []

  if (query.hasOwnProperty('Language')) {
    let join = `inner join ${dbConfig.tableLID} l
                 on v.${fieldMap.ID} = l.${fieldMap.RecordID}
                 and l.${fieldMap.Language} = '${query.Language}'`
    delete query.Language

    if (query.hasOwnProperty('LanguageScore')) {
      join += ` and l.${fieldMap.LanguageScore} >= ${query.LanguageScore}`
      delete query['LanguageScore']
    }
    joins.push(join)
  }

  if (query.hasOwnProperty('Speaker')) {
    let join = `inner join ${dbConfig.tableSID} s
                 on v.${fieldMap.ID} = s.${fieldMap.RecordID}
                 and s.${fieldMap.Speaker} = '${query.Speaker}'`
    delete query.Speaker

    if (query.hasOwnProperty('SpeakerScore')) {
      join += ` and s.${fieldMap.SpeakerScore} >= ${query.SpeakerScore}`
      delete query['SpeakerScore']
    }
    joins.push(join)
  }

  if (query.hasOwnProperty('Keyword')) {
    let join = `inner join ${dbConfig.tableKW} k
                 on v.${fieldMap.ID} = k.${fieldMap.RecordID}
                 and k.${fieldMap.KeywordScore} = (select max(${fieldMap.KeywordScore}) from ${dbConfig.tableKW} where v.${fieldMap.ID} = ${fieldMap.RecordID} and ${fieldMap.Keyword} = '${query.Keyword}')
                 and k.${fieldMap.InfoID} = (select max(${fieldMap.InfoID}) from ${dbConfig.tableKW} where v.${fieldMap.ID} = ${fieldMap.RecordID} and ${fieldMap.Keyword} = '${query.Keyword}')`
    delete query.Keyword

    if (query.hasOwnProperty('KeywordScore')) {
      join += ` and k.${fieldMap.KeywordScore} >= ${query.KeywordScore}`
      delete query['KeywordScore']
    }
    joins.push(join)
  }

  joins = joins.join(' ')

  /* Construct final sql statement */
  let sql

  if (query.hasOwnProperty('fromRow') && query.hasOwnProperty('toRow')) {
    sql = `select * from (
             select /*+ first_rows(${query.toRow - query.fromRow + 1}) */
             ${dbFields}, row_number() over (order by v.${fieldMap.ID}) rn
             from ${dbConfig.tableRecords} v
             ${joins}
             ${condition}
           ) where rn between ${query.fromRow} and ${query.toRow} order by rn`

    delete query.fromRow
    delete query.toRow
  } else {
    throw new Error('Row range not specified.')
  }

  /* error check */
  if (Object.keys(query).length !== 0) { // if query has any unexpected param
    throw new Error('Unexpected parameter in query.')
  }

  return sql
}

/* Construct a sql statement from a query for modifying a record */
/* the second argument is not used */
exports.buildSQL_ModifyRecord = (table, fields, req) => {
  const body = req.body
  const id = req.params.id

  let assignment = ''

  if (body.hasOwnProperty('Read')) {
    assignment = `${fieldMap.Read} = ${body.Read}`
  } else if (body.hasOwnProperty('Voice')) {
    assignment = `${fieldMap.VoiceHumanFlag} = ${body.Voice}`
  } else if (body.hasOwnProperty('Gender')) {
    assignment = `${fieldMap.GenderHumanFlag} = ${body.Gender}`
  } else {
    throw new Error('No expected parameter.')
  }

  return `update ${table} set ${assignment} where ${fieldMap.ID} = ${id}`
}

/* Construct a sql statement from a query for record info */
exports.buildSQL_RecordInfo = (table, fields, req) => {
  const id = req.params.id
  const dbFields = fields.map(field => fieldMap[field]).join(', ')

  return `select ${dbFields} from ${table} where ${fieldMap.RecordID} = ${id}`
}

/* Construct a sql statement from a query for changing language of a record */
exports.buildSQL_ModifyLanguage = (table, fields, req) => {
  return `update ${table}
          set ${fieldMap.LanguageHumanFlag} = 0, ${fieldMap.LanguageHumanResult} = '${req.body.Language}'
          where ${fieldMap.RecordID} = ${req.params.id} and ${fieldMap.Channel} = ${req.body.Channel}`
}

/* Construct a sql statement from a query for flag speaker of a record */
exports.buildSQL_FlagSpeaker = (table, fields, req) => {
  return `update ${table}
          set ${fieldMap.SpeakerHumanFlag} = '0'
          where ${fieldMap.InfoID} = ${req.body.InfoID}`
}

/* Construct a sql statement from a query for flag keyword of a record */
exports.buildSQL_FlagKeyword = (table, fields, req) => {
  return `update ${table}
          set ${fieldMap.KeywordHumanFlag} = '0'
          where ${fieldMap.InfoID} = ${req.body.InfoID}`
}

/* Construct a sql statement from a query for adding keyword */
/* the first two arguments are not used */
exports.buildSQL_AddKeyword = (table, fields, req) => {
  const query = req.query

  let dbFields_KWR = []
  let dbFields_KWS = []
  let values_KWR = []
  let values_KWS = []

  /* KEYWORDRECORD table */
  dbFields_KWR.push(fieldMap.SW_KeywordID)
  values_KWR.push(`'${query.Keyword}'`)

  dbFields_KWR.push(fieldMap.SW_KeywordStart)
  values_KWR.push(`'${query.KeywordStart}'`)

  dbFields_KWR.push(fieldMap.SW_KeywordEnd)
  values_KWR.push(`'${query.KeywordEnd}'`)

  dbFields_KWR.push(fieldMap.SW_Channel)
  values_KWR.push(query.Channel)

  dbFields_KWR.push(fieldMap.SW_Filename)
  values_KWR.push(`'${query.Filename}'`)

  /* KEYWORDS table */
  dbFields_KWS.push(fieldMap.SW_KeywordID)
  values_KWS.push(`'${query.Keyword}'`)

  dbFields_KWS.push(fieldMap.SW_Keyword)
  values_KWS.push(`'${query.Keyword}'`)

  return `insert all
    into ${dbConfig.tableSW_KWR} (${dbFields_KWR}) values (${values_KWR})
    into ${dbConfig.tableSW_KWS} (${dbFields_KWS}) values (${values_KWS})
    select 1 from dual`
}

/* Construct a sql statement from a query for adding speaker */
/* the first two arguments are unused */
exports.buildSQL_AddSpeaker = (table, fields, req) => {
  let query = req.query

  let dbFields_TGR = []
  let dbFields_TGS = []
  let values_TGR = []
  let values_TGS = []

  /* TARGETRECORDS table */
  dbFields_TGR.push(fieldMap.SID_SpeakerID)
  values_TGR.push(`'${query.Speaker}'`)

  dbFields_TGR.push(fieldMap.SID_Filename)
  values_TGR.push(`'${query.Filename}'`)

  /* TARGETS table */
  dbFields_TGS.push(fieldMap.SID_SpeakerID)
  values_TGS.push(`'${query.Speaker}'`)

  dbFields_TGS.push(fieldMap.SID_Speaker)
  values_TGS.push(`'${query.Speaker}'`)

  return `insert all
    into ${dbConfig.tableSID_TGR} (${dbFields_TGR}) values (${values_TGR})
    into ${dbConfig.tableSID_TGS} (${dbFields_TGS}) values (${values_TGS})
    select 1 from dual`
}

/* Construct a sql statement from a query for record continuous result */
exports.buildSQL_RecordContinous = (table, fields, req) => {
  const id = req.params.id
  const dbFields = fields.map(field => fieldMap[field]).join(', ')

  return `select ${dbFields} from ${table} where ${fieldMap.RecordID} = ${id}`
}

/* Construct a sql statement from a query for changing continuous result of a record */
exports.buildSQL_ModifyContinuous = (table, fields, req) => {
  return `update ${table}
          set ${fieldMap.SpeechHumanResult} = '${req.body.SpeechHumanResult}'
          where ${fieldMap.InfoID} = ${req.body.InfoID}`
}

/* build json from result of oracledb.ARRAY format */
exports.buildJSON = (fields, result) => {
  if (result === undefined) {
    return []
  } else {
    return result.map(row => {
      let item = {}
      for (let i = 0; i < row.length; i++) {
        item[fields[i]] = row[i]
      }
      return item
    })
  }
}
