const oracledb = require('oracledb')
const dbConfig = require('./db-config.js')

oracledb.autoCommit = true

/* Return result as array */
exports.execute = (sql, callback) => {
  oracledb.getConnection(
    {
      user          : dbConfig.user,
      password      : dbConfig.password,
      connectString : dbConfig.connectString
    },
    (err, connection) => {
      if (err) {
        console.error(err.message)
        callback(err)
        return
      }
      connection.execute(
        sql,
        (err, result) => {
          if (err) {
            console.error(err.message)
            doRelease(connection)
            callback(err)
            return
          }

          doRelease(connection)
          callback(null, result.rows)
        })
    }
  )
}

const doRelease = (connection) => {
  connection.release(
    (err) => {
      if (err) {
        console.error(err.message)
      }
    })
}
