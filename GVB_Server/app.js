const express = require('express')
const morgan = require('morgan')
const bodyParser = require('body-parser')

const api = require('./app/api')

const app = express()

app.use(morgan('dev'))
app.use(bodyParser.urlencoded({ extended: true }))

app.use('/', express.static(__dirname + '/public'))
app.use('/api', api)

// 404
app.use((req, res, next) => {
  const err = new Error('Not Found')
  err.status = 404
  next(err)
})

// error handlers
app.use((err, req, res, next) => {
  res.status(err.status || 500)
  res.send(err.status + ': ' + err.message)
})


module.exports = app
