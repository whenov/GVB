"use strict"

import React from 'react'
import { render } from 'react-dom'
import { Grid, Row, Col } from 'react-bootstrap'

require('file?name=[name].[ext]!./manage.html') // let webpack copy html file

render(
  <Grid>
      <Row style={{ marginTop: 20 }}>
        <Col md={12}>
        </Col>
      </Row>
  </Grid>,
  document.getElementById('root')
)
