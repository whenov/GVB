import 'whatwg-fetch'
import 'babel-polyfill'

export const CHANGE_FILTER = 'CHANGE_FILTER'
export const SELECT_PAGE = 'SELECT_PAGE'
export const RESET_DATATABLE = 'RESET_DATATABLE'
export const REQUEST_RECORDS = 'REQUEST_RECORDS'
export const RECEIVE_RECORDS = 'RECEIVE_RECORDS'
export const SELECT_ROW = 'SELECT_ROW'
export const SHOW_CHAT = 'SHOW_CHAT'
export const REQUEST_RECORD_CONTINUOUS = 'REQUEST_RECORD_CONTINUOUS'
export const RECEIVE_RECORD_CONTINUOUS = 'RECEIVE_RECORD_CONTINUOUS'
export const RESET_RECORD_CONTINUOUS = 'RESET_RECORD_CONTINUOUS'
export const REQUEST_RECORD_INFO = 'REQUEST_RECORD_INFO'
export const RECEIVE_RECORD_INFO = 'RECEIVE_RECORD_INFO'
export const SELECT_RECORD_INFO_TAB = 'SELECT_RECORD_INFO_TAB'
export const RESET_RECORD_INFO = 'RESET_RECORD_INFO'
export const SELECT_RECORD_INFO_ROW = 'SELECT_RECORD_INFO_ROW'
export const FOCUS_REGION = 'FOCUS_REGION'
export const RESET_FOCUS_REGION = 'RESET_FOCUS_REGION'
export const CREATE_AND_PLAY_REGION = 'CREATE_AND_PLAY_REGION'
export const RESET_CREATE_AND_PLAY_REGION = 'RESET_CREATE_AND_PLAY_REGION'

export const changeFilter = (name, value) => ({
  type: CHANGE_FILTER,
  name,
  value
})

export const selectPage = activePage => ({
  type: SELECT_PAGE,
  activePage
})

export const resetDataTable = () => ({
  type: RESET_DATATABLE
})

const requestRecords = () => ({
  type: REQUEST_RECORDS
})

const receiveRecords = json => ({
  type: RECEIVE_RECORDS,
  json
})

const buildQueryString = (params, pageCapacity, activePage, pageBuffNum) => {
  let arr = []

  // process date and time
  const processDateTime = (dateName, timeName, datetimeName) => {
    if (params.hasOwnProperty(dateName) && params[dateName]) { // params always has 'BeginTime'
      let date = params[dateName]
      let time = params[timeName]
      if (time.length === 7) {
        time = '0' + time // add leading zero
      }

      let datetime = date + 'T' + time

      arr.push(encodeURIComponent(datetimeName) + '=' + encodeURIComponent(datetime))
    }
  }
  processDateTime('BeginDate', 'BeginTime', 'BeginDateTime')
  processDateTime('EndDate', 'EndTime', 'EndDateTime')

  for (let key in params) {
    if (key === 'BeginDate' || key === 'EndDate' || key === 'BeginTime' || key === 'EndTime') {
      continue
    }

    if (params.hasOwnProperty(key)) {
      if (params[key] && params[key].trim()) {
        arr.push(encodeURIComponent(key) + '=' + encodeURIComponent(params[key]))
      }
    }
  }

  let toRow = activePage * pageCapacity
  let fromRow = toRow - pageCapacity + 1

  toRow += (pageBuffNum - 1) / 2 * pageCapacity
  fromRow -= (pageBuffNum - 1) / 2 * pageCapacity

  if (fromRow < 1) {
    const offset = 1 - fromRow
    fromRow += offset
    toRow += offset
  }

  arr.push('fromRow=' + fromRow)
  arr.push('toRow=' + toRow)

  return arr.join('&')
}

export const fetchRecords = () => (
  (dispatch, getState) => {
    const { filter, dataTable: { pageCapacity, activePage, pageBuffNum } } = getState()
    let query = buildQueryString(filter, pageCapacity, activePage, pageBuffNum)

    dispatch(requestRecords())

    fetch('/api/records?' + query)
      .then(response => response.json())
      .then(json => dispatch(receiveRecords(json)))
      .catch(err => {}) //TODO
  }
)

export const selectRow = id => ({
  type: SELECT_ROW,
  id
})

export const showChat = show => ({
  type: SHOW_CHAT,
  show
})

const requestRecordContinuous = () => ({
  type: REQUEST_RECORD_CONTINUOUS
})

const receiveRecordContinuous = json => ({
  type: RECEIVE_RECORD_CONTINUOUS,
  json
})

export const fetchRecordContinuous = () => (
  (dispatch, getState) => {
    const state = getState()
    const id = state.dataTable.activeRow

    if (id === -1) return

    dispatch(requestRecordContinuous())

    fetch(`/api/record/${id}/Continuous`)
      .then(response => response.json())
      .then(json => dispatch(receiveRecordContinuous(json)))
      .catch(err => {}) //TODO
  }
)

export const resetRecordContinuous = () => ({
  type: RESET_RECORD_CONTINUOUS
})

const requestRecordInfo = () => ({
  type: REQUEST_RECORD_INFO
})

const receiveRecordInfo = json => ({
  type: RECEIVE_RECORD_INFO,
  json
})

export const fetchRecordInfo = () => (
  (dispatch, getState) => {
    const state = getState()
    const id = state.dataTable.activeRow
    const infoType = state.recordInfo.tab

    if (id === -1) return

    dispatch(requestRecordInfo())

    fetch(`/api/record/${id}/${infoType}`)
      .then(response => response.json())
      .then(json => dispatch(receiveRecordInfo(json)))
      .catch(err => {}) //TODO
  }
)

export const selectRecordInfoTab = tab => ({
  type: SELECT_RECORD_INFO_TAB,
  tab
})

export const resetRecordInfo = () => ({
  type: RESET_RECORD_INFO
})

export const selectRecordInfoRow = id => ({
  type: SELECT_RECORD_INFO_ROW,
  id
})

export const focusRegion = () => ({
  type: FOCUS_REGION
})

export const resetFocusRegion = () => ({
  type: RESET_FOCUS_REGION
})

export const createAndPlayRegion = region => ({
  type: CREATE_AND_PLAY_REGION,
  region
})

export const resetCreateAndPlayRegion = () => ({
  type: RESET_CREATE_AND_PLAY_REGION
})
