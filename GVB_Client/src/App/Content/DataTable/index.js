import { connect } from 'react-redux'
import DataTable from './DataTable'
import { selectRow, fetchRecordInfo, showChat, fetchRecordContinuous } from '../../../actions'

const mapStateToProps = state => {
  const { activePage, pageCapacity, records, isFetching, activeRow } = state.dataTable

  const toRow = activePage * pageCapacity
  const fromRow = toRow - pageCapacity + 1

  let rows

  if (records.length === 0) {
    rows = []
  } else {
    const rowNumOffset = records[0].rowNum
    rows = records.slice(fromRow - rowNumOffset, toRow - rowNumOffset + 1)
  }

  let waitFetching

  if (rows.length === 0) { // empty results or isFetching
    waitFetching = isFetching
  } else { // hit cache
    waitFetching = false
  }

  return {
    showChat: state.chat.show,
    rows,
    activeRow,
    waitFetching
  }
}

const mapDispatchToProps = dispatch => ({
  onClickRow: e => {
    const id = Number(e.currentTarget.id)
    dispatch(selectRow(id))
    dispatch(fetchRecordInfo())
    dispatch(fetchRecordContinuous())
  },
  onDoubleClickRow: e => {
    const id = Number(e.currentTarget.id)
    dispatch(showChat(true))
    dispatch(fetchRecordContinuous())
  }
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DataTable)
