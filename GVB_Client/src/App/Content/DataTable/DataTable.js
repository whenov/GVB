import React, { Component } from 'react'
import { Row, Col, Table, Modal, ProgressBar, FormControl, FormGroup } from 'react-bootstrap'
import $ from 'jquery'
import 'whatwg-fetch'
import 'babel-polyfill'

class DataTable extends Component {
  componentDidUpdate() {
    // need update if load data in the first render
    $('#DataTable > tbody > tr').off('click').on('click', this.props.onClickRow)
    $('#DataTable > tbody > tr select').off('click').on('click', e => e.stopPropagation())
    $('#DataTable > tbody > tr').off('dblclick').on('dblclick', this.props.onDoubleClickRow)
  }

  render() {
    const { showChat, rows, activeRow, waitFetching } = this.props

    return (
      <div>
      <Row>
        <Col md={12}>
          <Table bordered condensed id='DataTable'>
            <thead>
              <tr>
                <th>文件名</th>
                <th>时间</th>
                <th>阅报</th>
                {showChat ? null : <th>是否话音</th>}
                {showChat ? null : <th>话音分数</th>}
                {showChat ? null : <th>性别</th>}
                {showChat ? null : <th>性别分数</th>}
                {rows.length > 0 && rows[0].hasOwnProperty('Language') ? <th>语言</th> : null}
                {rows.length > 0 && rows[0].hasOwnProperty('LanguageScore') ? <th>语言分数</th> : null}
                {rows.length > 0 && rows[0].hasOwnProperty('Speaker') ? <th>说话人</th> : null}
                {rows.length > 0 && rows[0].hasOwnProperty('SpeakerScore') ? <th>说话人分数</th> : null}
                {rows.length > 0 && rows[0].hasOwnProperty('Keyword') ? <th>关键字</th> : null}
                {rows.length > 0 && rows[0].hasOwnProperty('KeywordScore') ? <th>关键字分数</th> : null}
              </tr>
            </thead>
            <tbody>
            {(() => (
              rows.length === 0 ?
                <tr><td colSpan="7" style={{ textAlign: 'center' }}>无数据</td></tr>
              :
                rows.map(record => (
                  <tr key={record.ID} id={record.ID} className={(activeRow === record.ID) ? 'active' : ''}>
                    <td>{record.Filename}</td>
                    <td>{typeof record.FileDateTime === 'string' ? (record.FileDateTime.substr(0, 10) + ' ' + record.FileDateTime.substr(11, 8)) : null}</td>
                    <td>
                      <FormGroup bsSize="small">
                        <FormControl componentClass="select" name="Read" onChange={this.editRecord.bind(this)} defaultValue={record.Read}>
                          <option value={0}>未阅</option>
                          <option value={1}>已阅</option>
                        </FormControl>
                      </FormGroup>
                    </td>
                    {showChat ? null : <td>
                      <FormGroup bsSize="small">
                        <FormControl componentClass="select" name="Voice" onChange={this.editRecord.bind(this)}
                          defaultValue={record.VoiceHumanFlag !== 2 /* change to -1 in production */ ? record.VoiceHumanFlag : record.Voice}>
                          <option value={0}>非话音</option>
                          <option value={1}>话音</option>
                        </FormControl>
                      </FormGroup>
                    </td>}
                    {showChat ? null : <td>{record.VoiceHumanFlag !== 2 /* change to -1 in production */ ? 100 : record.VoiceScore}</td>}
                    {showChat ? null : <td>
                      <FormGroup bsSize="small">
                        <FormControl componentClass="select" name="Gender" onChange={this.editRecord.bind(this)}
                          defaultValue={record.GenderHumanFlag !== 2 /* change to -1 in production */ ? record.GenderHumanFlag : record.Gender}>
                          <option value={0}>男</option>
                          <option value={1}>女</option>
                        </FormControl>
                      </FormGroup>
                    </td>}
                    {showChat ? null : <td>{record.GenderHumanFlag !== 2 /* change to -1 in production */ ? 100 : record.GenderScore}</td>}
                    {!showChat && record.hasOwnProperty('Language') ? <td>{record.Language}</td> : null}
                    {!showChat && record.hasOwnProperty('LanguageScore') ? <td>{record.LanguageScore}</td> : null}
                    {!showChat && record.hasOwnProperty('Speaker') ? <td>{record.Speaker}</td> : null}
                    {!showChat && record.hasOwnProperty('SpeakerScore') ? <td>{record.SpeakerScore}</td> : null}
                    {!showChat && record.hasOwnProperty('Keyword') ? <td>{record.Keyword}</td> : null}
                    {!showChat && record.hasOwnProperty('KeywordScore') ? <td>{record.KeywordScore}</td> : null}
                  </tr>
                ))
            ))()}
            </tbody>
          </Table>
        </Col>
      </Row>

      <Modal show={waitFetching} animation={false} backdrop="static">
        <Modal.Header>
          <Modal.Title>载入中…</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ProgressBar active now={100} />
        </Modal.Body>
      </Modal>
      </div>
    )
  }

  editRecord(e) {
    const id = e.target.parentNode.parentNode.parentNode.id
    const name = e.target.name
    const value = e.target.options[e.target.selectedIndex].value

    fetch(`/api/record/${id}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: `${name}=${value}`
      })
      .then(response => {})
      .catch(err => {})
  }
}

export default DataTable
