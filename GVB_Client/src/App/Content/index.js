import { connect } from 'react-redux'
import Content from './Content'

const mapStateToProps = state => {
  return {
    show: state.chat.show
  }
}

export default connect(
  mapStateToProps
)(Content)
