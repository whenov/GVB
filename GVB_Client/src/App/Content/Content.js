import React, { Component } from 'react'
import { Grid, Row, Col } from 'react-bootstrap'
import DataTable from './DataTable'
import Chat from './Chat'

class Content extends Component {
  render() {
    return (
      <Row>
        <Col md={this.props.show ? 6 : 12}>
          <DataTable />
        </Col>
        { this.props.show ?
        <Col md={6}>
          <Chat />
        </Col>
        : null }
      </Row>
    )
  }
}

export default Content
