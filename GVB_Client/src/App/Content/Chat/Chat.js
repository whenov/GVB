import React, { Component } from 'react'
import { Well, Button, FormGroup, FormControl } from 'react-bootstrap'
import 'whatwg-fetch'
import 'babel-polyfill'
import $ from 'jquery'

class Chat extends Component {
  constructor(props) {
    super(props)

    this.state = {
      editingItemID: -1,
      editingItemText: ''
    }
  }

  render() {
    const { speeches } = this.props

    const getText = (speech) => (speech.SpeechHumanResult ? speech.SpeechHumanResult : speech.Speech)

    return (
      <Well className="chat">
        <Button className="close" onClick={this.props.closeChat}>&times;</Button>
        {
          speeches.length === 0 ? null :
          speeches
            .sort((a, b) => (a.SpeechStart - b.SpeechStart + 0.0001 * (a.Channel - b.Channel))) // add a small decimal to sort for translation demostration
            .map(speech => (
            <div key={speech.InfoID} className={"bubble " + (speech.Channel === 1 ? "me" : "you")}>

              <span className="glyphicon glyphicon-play" onClick={e => this.props.createAndPlayRegion({start: speech.SpeechStart, end: speech.SpeechEnd})}></span>
              &nbsp;
              <span onClick={e => this.setState({editingItemID: speech.InfoID, editingItemText: getText(speech)})}>
              {this.state.editingItemID === speech.InfoID
              ? <FormGroup bsSize="small">
                  <FormControl type="text" autoFocus id="modifyContinuous"
                    value={this.state.editingItemText}
                    onBlur={e => this.setState({editingItemID: -1, editingItemText: ''})}
                    onChange={e => this.setState({editingItemText: e.target.value})}
                    onKeyUp={this.modifyContinuous.bind(this)} />
                </FormGroup>
              : getText(speech)}
              </span>
            </div>
          ))
        }
      </Well>
    )
  }

  modifyContinuous(e) {
    if (e.keyCode === 13) {
      $('#modifyContinuous').blur()

      fetch(`/api/record/${this.props.recordID}/Continuous`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          body: `InfoID=${this.state.editingItemID}&SpeechHumanResult=${this.state.editingItemText}`
        })
        .then(response => {this.props.refetchRecordContinuous()})
        .catch(err => {})
    }
  }
}

export default Chat
