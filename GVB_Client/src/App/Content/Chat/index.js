import { connect } from 'react-redux'
import Chat from './Chat'
import { showChat, fetchRecordContinuous, createAndPlayRegion } from '../../../actions'

const mapStateToProps = state => (
  state.chat
)

const mapDispatchToProps = dispatch => ({
  refetchRecordContinuous: () => {
    dispatch(fetchRecordContinuous())
  },
  createAndPlayRegion: region => {
    dispatch(createAndPlayRegion(region))
  },
  closeChat: () => {
    dispatch(showChat(false))
  }
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Chat)
