import React from 'react'
import { Navbar, Nav, NavItem } from 'react-bootstrap'

const Navigation = ({ page }) => (
  <Navbar staticTop id="navbar">
    <Navbar.Header>
      <Navbar.Brand>
        <a href="#">语音智能处理系统</a>
      </Navbar.Brand>
    </Navbar.Header>
    <Nav>
      <NavItem eventKey={1} href="/" className={page === 1 ? "active" : ""}>语音提报</NavItem>
      <NavItem eventKey={2} href="/manage" className={page === 2 ? "active" : ""}>资料管理</NavItem>
    </Nav>
  </Navbar>
)

export default Navigation
