import React from 'react'
import { Grid, Row, Col } from 'react-bootstrap'
import Navigation from './Navigation'
import Footer from './Footer'
import FilterPanel from './FilterPanel'
import Content from './Content'

require('bootstrap/dist/css/bootstrap.min.css')
require('../style.scss')

const App = () => (
  <div>
  <Navigation page={1} />
  <Grid id="content">

    <Row>
      <Col md={12}>
        <FilterPanel />
      </Col>
    </Row>

    <Row style={{ marginTop: 10 }}>
      <Col md={12}>
        <Content />
      </Col>
    </Row>

  </Grid>
  <Footer />
  </div>
)

export default App
