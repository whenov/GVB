import React, { Component } from 'react'
import { Table, Nav, NavItem, FormControl, FormGroup } from 'react-bootstrap'
import 'whatwg-fetch'
import 'babel-polyfill'
import $ from 'jquery'

class RecordInfo extends Component {
  constructor(props) {
    super(props)

    this.state = {
      editingItemID: -1,
      editingItemChannel: 0,
      editingItemText: ''
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { tab, infos, isFetching, activeRow } = this.props.recordInfo
    if (isFetching === false && infos !== prevProps.recordInfo.infos && infos.length > 0 && tab === 'Keyword') {
      $('#RecordInfo > tbody > tr').off('click').on('click', this.props.onClickRow)
    }
    if (activeRow > -1 && activeRow !== prevProps.recordInfo.activeRow) {
      $('#RecordInfo > tbody > tr.active').get(0).scrollIntoView()
    }
  }

  render() {
    const { tab, infos, isFetching, activeRow } = this.props.recordInfo
    const { onSelect } = this.props

    return (
      <div>
        <Nav bsStyle="pills" activeKey={tab} onSelect={onSelect}>
          <NavItem eventKey="Language">语种</NavItem>
          <NavItem eventKey="Speaker">说话人</NavItem>
          <NavItem eventKey="Keyword">关键字</NavItem>
        </Nav>
        <Table condensed id="RecordInfo">
          <tbody>
          {(() => {
            if (isFetching === true) {
              return <tr><td style={{ textAlign: 'center', width: 360 }}>载入中…</td></tr>
            } else if (infos.length === 0) {
              return <tr><td style={{ textAlign: 'center', width: 360 }}>无数据</td></tr>
            } else if (tab === 'Keyword') {
              return infos.filter(item => item.KeywordHumanFlag !== '0').map(item => (
                <tr key={item.InfoID} id={item.InfoID} className={(activeRow === item.InfoID) ? 'active' : ''}>
                  <td>{item['Keyword']}</td>
                  <td>{item['KeywordStart'].toFixed(2) + '~' + item['KeywordEnd'].toFixed(2)}</td>
                  <td>{item['KeywordScore']}</td>
                  <td>{['', 'L', 'R'][item.Channel]}</td>
                  <td><button type="button" className="close" onClick={this.flagKeyword.bind(this)}>&times;</button></td>
                </tr>
              ))
            } else if (tab === 'Language') {
              const langL = infos.filter(a => (a.Channel === 1)).sort((a, b) => (b.LanguageScore - a.LanguageScore))[0]
              const langR = infos.filter(a => (a.Channel === 2)).sort((a, b) => (b.LanguageScore - a.LanguageScore))[0]

              let trs = []

              if (langL) {
                if (langL.LanguageHumanFlag === 0) { // flagged as wrong
                  langL.Language = langL.LanguageHumanResult.trim()
                  langL.LanguageScore = 100
                }
                const trL = (
                  <tr key={langL.InfoID} id={langL.InfoID}>
                    <td onClick={e => this.setState({editingItemID: langL.InfoID, editingItemChannel: langL.Channel, editingItemText: langL['Language']})}>
                      {this.state.editingItemID === langL.InfoID
                      ? <FormGroup bsSize="small">
                          <FormControl type="text" autoFocus id="modifyLanguage"
                            value={this.state.editingItemText}
                            onBlur={e => this.setState({editingItemID: -1, editingItemChannel: 0, editingItemText: ''})}
                            onChange={e => this.setState({editingItemText: e.target.value})}
                            onKeyUp={this.editLanguage.bind(this)} />
                        </FormGroup>
                      : langL['Language']}</td>
                    <td>{langL['LanguageScore']}</td>
                    <td>L</td>
                  </tr>
                )
                trs.push(trL)
              }

              if (langR) {
                if (langR.LanguageHumanFlag === 0) { // flagged as wrong
                  langR.Language = langR.LanguageHumanResult.trim()
                  langR.LanguageScore = 100
                }
                const trR = (
                  <tr key={langR.InfoID} id={langR.InfoID}>
                    <td onClick={e => this.setState({editingItemID: langR.InfoID, editingItemChannel: langR.Channel, editingItemText: langR['Language']})}>
                      {this.state.editingItemID === langR.InfoID
                      ? <FormGroup bsSize="small">
                          <FormControl type="text" autoFocus id="modifyLanguage"
                            value={this.state.editingItemText}
                            onBlur={e => this.setState({editingItemID: -1, editingItemChannel: 0, editingItemText: ''})}
                            onChange={e => this.setState({editingItemText: e.target.value})}
                            onKeyUp={this.editLanguage.bind(this)} />
                        </FormGroup>
                      : langR['Language']}</td>
                    <td>{langR['LanguageScore']}</td>
                    <td>R</td>
                  </tr>
                )
                if (trR) trs.push(trR)
              }

              return trs
              } else {
              return infos.filter(item => item.SpeakerHumanFlag !== '0').map(item => (
                <tr key={item.InfoID} id={item.InfoID}>
                  <td>{item[tab]}</td>
                  <td>{item[tab + 'Score']}</td>
                  <td>{['', 'L', 'R'][item.Channel]}</td>
                  <td><button type="button" className="close" onClick={this.flagSpeaker.bind(this)}>&times;</button></td>
                </tr>
              ))
            }
          })()}
          </tbody>
        </Table>
      </div>
    )
  }

  editLanguage(e) {
    if (e.keyCode === 13) {
      $('#modifyLanguage').blur()

      fetch(`/api/record/${this.props.recordID}/Language`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          body: `Language=${this.state.editingItemText}&Channel=${this.state.editingItemChannel}`
        })
        .then(response => {this.props.refetchRecordInfo()})
        .catch(err => {})
    }
  }

  flagSpeaker(e) {
    const InfoID = e.target.parentNode.parentNode.id
    fetch(`/api/record/${this.props.recordID}/Speaker`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: `InfoID=${InfoID}`
      })
      .then(response => {this.props.refetchRecordInfo()})
      .catch(err => {})
  }

  flagKeyword(e) {
    const InfoID = e.target.parentNode.parentNode.id
    fetch(`/api/record/${this.props.recordID}/Keyword`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: `InfoID=${InfoID}`
      })
      .then(response => {this.props.refetchRecordInfo()})
      .catch(err => {})
  }
}

export default RecordInfo
