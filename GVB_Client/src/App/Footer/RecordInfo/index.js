import { connect } from 'react-redux'
import RecordInfo from './RecordInfo'
import { selectRecordInfoTab, fetchRecordInfo, selectRecordInfoRow, focusRegion } from '../../../actions'

const mapStateToProps = state => ({
  recordID: state.dataTable.activeRow,
  recordInfo: state.recordInfo
})

const mapDispatchToProps = dispatch => ({
  refetchRecordInfo: () => {
    dispatch(fetchRecordInfo())
  },
  onSelect: eventKey => {
    dispatch(selectRecordInfoTab(eventKey))
    dispatch(fetchRecordInfo())
  },
  onClickRow: e => {
    const id = Number(e.currentTarget.id)
    dispatch(selectRecordInfoRow(id))
    dispatch(focusRegion())
  }
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RecordInfo)
