import React from 'react'
import { Grid, Row, Col } from 'react-bootstrap'
import Player from './Player'
import RecordInfo from './RecordInfo'

const Footer = () => (
  <footer>
    <Grid>
      <Row>
        <Col md={8}>
          <Player />
        </Col>
        <Col md={4}>
          <RecordInfo />
        </Col>
      </Row>
    </Grid>
  </footer>
)

export default Footer
