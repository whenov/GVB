import { connect } from 'react-redux'
import Player from './Player'
import { selectRecordInfoRow, resetFocusRegion, resetCreateAndPlayRegion } from '../../../actions'

const mapStateToProps = state => {
  const record = state.dataTable.records.find(record => (record.ID === state.dataTable.activeRow))
  const filename = record ? record.Filename : undefined

  const info = state.recordInfo.infos.find(info => (info.InfoID === state.recordInfo.activeRow))
  const moment = info ? info.KeywordStart : undefined

  return {
    recordInfo: state.recordInfo,
    filename,
    focusRegion: state.player.focusRegion,
    moment,
    regionToCreateAndPlay: state.player.regionToCreateAndPlay
  }
}

const mapDispatchToProps = dispatch => ({
  resetFocusRegion: () => {
    dispatch(resetFocusRegion())
  },
  resetCreateAndPlayRegion: () => {
    dispatch(resetCreateAndPlayRegion())
  },
  onRegionClick: (region, e) => {
    dispatch(selectRecordInfoRow(region.id))
  },
  onRegionIn: region => {
    dispatch(selectRecordInfoRow(region.id))
  }
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Player)
