import React, { Component } from 'react'
import { Grid, Row, Col, Button, ButtonToolbar, ButtonGroup, ProgressBar, FormControl, FormGroup, Modal } from 'react-bootstrap'
import 'whatwg-fetch'
import 'babel-polyfill'
import $ from 'jquery'

const WaveSurfer = require('exports?WaveSurfer!./wavesurfer.js/src/wavesurfer.js')

window.WaveSurfer = WaveSurfer
require('imports?WaveSurfer=>window.WaveSurfer!./wavesurfer.js/src/util.js')
require('imports?WaveSurfer=>window.WaveSurfer!./wavesurfer.js/src/drawer.js')
require('imports?WaveSurfer=>window.WaveSurfer!./wavesurfer.js/src/drawer.multicanvas.js')
require('imports?WaveSurfer=>window.WaveSurfer!./wavesurfer.js/src/webaudio.js')
require('imports?WaveSurfer=>window.WaveSurfer!./wavesurfer.js/plugin/wavesurfer.timeline.js')
require('imports?WaveSurfer=>window.WaveSurfer!./wavesurfer.js/plugin/wavesurfer.regions.js')
delete window.WaveSurfer

/*
 * To keep play/pause, volume, zoom level and channel consistent in this.state
 * and this.wavesurfer, different strategy is used:
 * 1. play/pause: setState() in wavesurfer event handler. (Because the audio would end playing sometime)
 * 2. volume: setVolume(), then setState() according to getVolume(). (No particular reason)
 * 3. zoom level: setState() in wavesurfer event handler. (No particular reason)
 * 4. channel: setState(), then setChannel(). (No particular reason)
 */

class Player extends Component {
  constructor(props) {
    super(props)

    fetch('/api/audioserver')
      .then(response => response.text())
      .then(text => {this.audioServer = text})
      .catch(err => {})

    this.initialMinPxPerSec = 60 // canvas width cannot be larger than 32767

    this.state = {
      isLoading: false,
      channel: 0, // 0: LR, 1: L, 2: R
      numberOfChannels: 2,
      volume: 1,
      minPxPerSec: this.initialMinPxPerSec,

      /* modal */
      enableCopy: false,
      showModal: false,

      /* add keyword */
      enableAddKeyword: false,
      addedKeyword: '',
      addKeywordSuccess: null,
      addedRegion: null,

      /* add speaker */
      enableAddSpeaker: false,
      addedSpeaker: '',
      addSpeakerSuccess: null
    }
  }

  componentDidMount() {
    const wavesurfer = this.wavesurfer = WaveSurfer.create({
      container: '#wavesurfer',
      progressColor: '#255F92',
      waveColor: '#2F79B9',
      minPxPerSec: this.initialMinPxPerSec,
      fillParent: false,
      scrollParent: true,
      renderer: 'MultiCanvas'
    })

    /* plugins */
    wavesurfer.on('ready', () => {
      let timeline = Object.create(WaveSurfer.Timeline)

      timeline.init({
        wavesurfer: wavesurfer,
        container: "#wavesurfer-timeline"
      })
    })

    /* sync volume with wavesurfer backend */
    this.setState({ volume: wavesurfer.backend.getVolume() })

    /* setState on events */
    wavesurfer.on('play', () => this.setState({ playing: true }))
    wavesurfer.on('pause', () => this.setState({ playing: false }))
    wavesurfer.on('zoom', minPxPerSec => this.setState({ minPxPerSec: minPxPerSec }))

    /* annotation tool */
    wavesurfer.enableDragSelection({id: -1, drag: false, color: 'rgba(255, 130, 7, 0.1)'})
    wavesurfer.on('region-click', this.props.onRegionClick)
    wavesurfer.on('region-in', this.props.onRegionIn)
    wavesurfer.on('region-created', region => {
      if (region.id === -1) {
        if (this.props.recordInfo.tab !== 'Keyword' || this.state.channel === 0) { // not allowed to add keyword if not in Keyword tab or both channel are on
          region.remove()
          return
        }

        if (this.state.addedRegion) this.state.addedRegion.remove()

        this.setState({
          enableAddKeyword: true,
          addedKeyword: '',
          addKeywordSuccess: null,
          addedRegion: region
        })
        $('#addKeyword').focus()
      }
    })
  }

  componentDidUpdate(prevProps, prevState) {
    const { filename, recordInfo: { tab, infos, isFetching }, focusRegion, moment, regionToCreateAndPlay } = this.props
    const { channel } = this.state
    const wavesurfer = this.wavesurfer

    /* load audio file if selected row changed */
    if (filename !== prevProps.filename) {
      wavesurfer.clearRegions()
      wavesurfer.empty()

      if (filename === undefined) {
        this.setState({
          enableAddSpeaker: false,
          enableCopy: false
        })
      } else {
        if (prevProps.filename) wavesurfer.setChannel(-1)

        wavesurfer.load(this.audioServer + filename)
        this.setState({ isLoading: true })

        wavesurfer.once('ready', () => {
          this.setState({ isLoading: false })

          const numberOfChannels = wavesurfer.backend.buffer.numberOfChannels
          this.setState({numberOfChannels})
          if (numberOfChannels === 1) {
            this.setState({channel: 1})
          } else if (numberOfChannels === 2) {
            this.setState({channel: 0})
          }
        })

        this.setState({
          enableAddSpeaker: true,
          enableCopy: true
        })
      }

      this.setState({
        enableAddKeyword: false,
        addedKeyword: '',
        addKeywordSuccess: null,
        addedRegion: null,
        addedSpeaker: '',
        addSpeakerSuccess: null
      })
    }

    /* add regions of keywords */
    const regionCreator = (id, start, end) => ({
      id,
      start,
      end,
      loop: false,
      drag: false,
      resize: false,
      color: 'rgba(50, 205, 50, 0.1)'
    })

    const loadRegionsFromInfos = () => {
      wavesurfer.clearRegions()
      infos.filter(info => info.KeywordHumanFlag !== '0').forEach(info => {
        if (channel === 0 || channel === info.Channel) // channel representation in database: 1: left, 2: right
          wavesurfer.addRegion(regionCreator(info.InfoID, info.KeywordStart, info.KeywordEnd))
      })
    }

    if (tab === 'Keyword' && isFetching === false && prevProps.recordInfo.isFetching === true) {
      if (this.state.isLoading) {
        wavesurfer.once('ready', () => {
          loadRegionsFromInfos()
        })
      } else {
        loadRegionsFromInfos()
      }
    } else if (tab !== prevProps.recordInfo.tab && tab !== 'Keyword') {
      wavesurfer.clearRegions()
    }

    if (focusRegion) {
      wavesurfer.play(moment)
      wavesurfer.pause()
      this.props.resetFocusRegion()
    }

    if (regionToCreateAndPlay) {
      if (this.region) this.region.remove()
      let region = regionToCreateAndPlay
      region.id = -2
      region.loop = true
      region.drag = false
      region.resize = false
      region.color = 'rgba(205, 50, 50, 0.1)'
      this.region = wavesurfer.addRegion(region)
      this.region.play()
      this.props.resetCreateAndPlayRegion()
    }

    if (channel !== prevState.channel) {
      wavesurfer.setChannel(channel - 1)
    }
  }

  render() {
    const { infos, isFetching } = this.props.recordInfo
    const { channel, numberOfChannels } = this.state
    const wavesurfer = this.wavesurfer

    return (
      <div>
        <Row>
          <Col md={12}>
            <div id="wavesurfer">
              <ProgressBar active now={100} style={{display: this.state.isLoading ? "" : "none"}}/>
            </div>
            <div id="wavesurfer-timeline"></div>
          </Col>
        </Row>
        <Row>
          <Col md={12}>
            {/*
              * Anonymous wrapper function added intentionally in next line,
              * for this.wavesurfer is not defined util this Component is mounted.
              */}
            <button type="button" className="player" onClick={() => wavesurfer.playPause()}>
              <span ref="playPauseIcon" className={"glyphicon " + (this.state.playing ? "glyphicon-pause" : "glyphicon-play")} aria-hidden="true"></span>
            </button>

            <ButtonGroup bsSize="xsmall" className="player">
              <Button active={channel === 1 || numberOfChannels === 1} onClick={() => this.setState({ channel: 1 })}>L</Button>
              <Button active={channel === 0 && numberOfChannels > 1} onClick={() => this.setState({ channel: 0 })} disabled={numberOfChannels === 1}>LR</Button>
              <Button active={channel === 2} onClick={() => this.setState({ channel: 2 })} disabled={numberOfChannels === 1}>R</Button>
            </ButtonGroup>

            <div className="player">
              <span className="glyphicon glyphicon-volume-down" aria-hidden="true"></span>
              <input type="range"
                value={this.state.volume}
                min="0" max="1" step="0.1"
                onChange={() => {}} // add empty onChange handler to surpress react warning
                onInput={e => {
                  wavesurfer.setVolume(Number(e.target.value))
                  this.setState({volume: wavesurfer.backend.getVolume()})
                }} />
            </div>

            <div className="player">
              <span className="glyphicon glyphicon-zoom-out" aria-hidden="true"></span>
              <input type="range"
                value={this.state.minPxPerSec}
                min={this.initialMinPxPerSec}
                max={400}
                onChange={() => {}} // add empty onChange handler to surpress react warning
                onInput={e => wavesurfer.zoom(Number(e.target.value))} />
              <span className="glyphicon glyphicon-zoom-in" aria-hidden="true"></span>
            </div>

            <div className="player">
              <FormGroup validationState={this.state.addKeywordSuccess === null ? null : (this.state.addKeywordSuccess === true ? 'success' : 'error')}>
                <FormControl type="text" placeholder="关键字" id="addKeyword"
                  disabled={!this.state.enableAddKeyword}
                  value={this.state.addedKeyword}
                  onChange={e => this.setState({addedKeyword: e.target.value})}
                  onKeyUp={this.onAddKeyword.bind(this)} />
                <FormControl.Feedback />
              </FormGroup>
            </div>

            <div className="player">
              <FormGroup validationState={this.state.addSpeakerSuccess === null ? null : (this.state.addSpeakerSuccess === true ? 'success' : 'error')}>
                <FormControl type="text" placeholder="说话人" id="addSpeaker"
                  disabled={!this.state.enableAddSpeaker}
                  value={this.state.addedSpeaker}
                  onChange={e => this.setState({addedSpeaker: e.target.value, addSpeakerSuccess: null})}
                  onKeyUp={this.onAddSpeaker.bind(this)} />
                <FormControl.Feedback />
              </FormGroup>
            </div>

            <div className="player">
              <Button disabled={!this.state.enableCopy} onClick={() => this.setState({ showModal: true })}>抄报</Button>
            </div>

            <Modal show={this.state.showModal} onHide={() => this.setState({ showModal: false })}>
              <Modal.Header closeButton>
                <Modal.Title>抄报</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <FormControl componentClass="textarea" id="textarea" />
              </Modal.Body>
              <Modal.Footer>
                <Button href={'' + this.audioServer + this.props.filename} download>下载语音</Button>
                <Button bsStyle="primary" onClick={this.saveTextAsFile.bind(this)}>保存</Button>
                <Button onClick={() => this.setState({ showModal: false })}>取消</Button>
              </Modal.Footer>
            </Modal>

          </Col>
        </Row>
      </div>
    )
  }

  saveTextAsFile() {
    var textToWrite = document.getElementById('textarea').value
    var textFileAsBlob = new Blob([textToWrite], {
      type: 'text/plain'
    })
    var fileNameToSaveAs = this.props.filename + '.txt'

    var downloadLink = document.createElement('a')
    downloadLink.download = fileNameToSaveAs
    downloadLink.innerHTML = 'Download File'
    if (window.webkitURL != null) {
      // Chrome allows the link to be clicked
      // without actually adding it to the DOM.
      downloadLink.href = window.webkitURL.createObjectURL(textFileAsBlob)
    } else {
      // Firefox requires the link to be added to the DOM
      // before it can be clicked.
      downloadLink.href = window.URL.createObjectURL(textFileAsBlob)
      downloadLink.onclick = destroyClickedElement
      downloadLink.style.display = 'none'
      document.body.appendChild(downloadLink)
    }

    downloadLink.click()
  }

  onAddKeyword(e) {
    const { addedKeyword, addedRegion, channel } = this.state
    if (e.keyCode === 13) {
      $('#addKeyword').blur()

      const query =
        `Keyword=${addedKeyword}&KeywordStart=${addedRegion.start.toFixed(3)}&KeywordEnd=${addedRegion.end.toFixed(3)}&Channel=${channel}&Filename=${this.props.filename}`
      fetch('/api/keyword?' + query, {method: 'POST'})
        .then(response => this.setState({
            addKeywordSuccess: true,
            enableAddKeyword: false
          }))
        .catch(err => this.setState({addKeywordSuccess: false}))
    }
  }

  onAddSpeaker(e) {
    const { addedSpeaker } = this.state
    if (e.keyCode === 13) {
      $('#addSpeaker').blur()

      const query =
        `Speaker=${addedSpeaker}&Filename=${this.props.filename}`
      fetch('/api/speaker?' + query, {method: 'POST'})
        .then(response => this.setState({addSpeakerSuccess: true}))
        .catch(err => this.setState({addSpeakerSuccess: false}))
    }
  }
}

export default Player
