import React, { Component } from 'react'
import { Row, Col, Button, Collapse, Well } from 'react-bootstrap'
import FilterForm from './FilterForm'
import DataTablePagination from './DataTablePagination'

class FilterPanel extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  toggleCollapse() {
    this.setState({ open: !this.state.open })
  }

  render() {
    const filter = this.props.filter

    return (
      <div>
      <Row>
        <Col md={1}>
          <Button bsStyle="primary" onClick={this.toggleCollapse.bind(this)}>
            筛选条件
          </Button>
        </Col>
        <Col md={7}>
          <ul className="list-inline" style={{ "height": 20, "marginTop": 7 }}>
          { filter.BeginDate ? <li>开始：{filter.BeginDate}</li> : null }
          { (filter.BeginDate && filter.BeginTime) ? <li>{filter.BeginTime}</li> : null }
          { filter.EndDate ? <li>截至：{filter.EndDate}</li> : null }
          { (filter.EndDate && filter.EndTime) ? <li>{filter.EndTime}</li> : null }
          { filter.Read ? (filter.Read == 0 ? <li>未阅</li> : <li>已阅</li>) : null }
          { filter.Voice ? (
              filter.Voice == 0 ? <li>非话音{ filter.VoiceScore ? ('：' + filter.VoiceScore) : null }</li>
              : <li>话音{ filter.VoiceScore ? ('：' + filter.VoiceScore) : null }</li>
            ) : null }
          { filter.Gender ? (
              filter.Gender == 0 ? <li>男{ filter.GenderScore ? ('：' + filter.GenderScore) : null }</li>
              : <li>女{ filter.GenderScore ? ('：' + filter.GenderScore) : null }</li>
            ) : null }
          { filter.Language ? (
              <li>{filter.Language + (filter.LanguageScore ? '：' + filter.LanguageScore : '')}</li>
            ) : null }
          { filter.Speaker ? (
              <li>{filter.Speaker + (filter.SpeakerScore ? '：' + filter.SpeakerScore : '')}</li>
            ) : null }
          { filter.Keyword ? (
              <li>{filter.Keyword + (filter.KeywordScore ? '：' + filter.KeywordScore : '')}</li>
            ) : null }
          </ul>
        </Col>
        <Col md={4}>
          <DataTablePagination />
        </Col>
      </Row>
      <Row>
        <Col md={12}>
          <Collapse in={this.state.open} style={{ marginTop: 10 }}>
            <div> {/* div wrapper is required for a smooth collapse */}
              <Well>
                <FilterForm toggleCollapse={this.toggleCollapse.bind(this)} />
              </Well>
            </div>
          </Collapse>
        </Col>
      </Row>
      </div>
    )
  }
}

export default FilterPanel
