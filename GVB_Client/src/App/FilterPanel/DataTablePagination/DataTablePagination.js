import React from 'react'
import { Pagination } from 'react-bootstrap'

const DataTablePagination = ({ activePage, maxPage, pageBuffNum, onSelect }) => (
  <Pagination prev next maxButtons={pageBuffNum} className="pull-right"
    items={maxPage} activePage={activePage} onSelect={onSelect} />
)

export default DataTablePagination
