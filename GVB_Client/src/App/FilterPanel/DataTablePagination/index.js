import { connect } from 'react-redux'
import DataTablePagination from './DataTablePagination'
import { selectPage, fetchRecords, resetRecordInfo, showChat } from '../../../actions'

const mapStateToProps = state => {
  const { activePage, pageCapacity, pageBuffNum, records } = state.dataTable

  let maxPage

  if (records.length === 0) {
    maxPage = 1
  } else {
    maxPage = Math.ceil(records[records.length - 1].rowNum / pageCapacity)

    // dirty fix of flash of pagination when select previous page
    const rightestPage = activePage + (pageBuffNum - 1) / 2
    if (maxPage > pageBuffNum && maxPage > rightestPage)
      maxPage = rightestPage
  }

  return {
    activePage,
    maxPage,
    pageBuffNum
  }
}

const mapDispatchToProps = dispatch => ({
  onSelect: eventKey => {
    dispatch(resetRecordInfo())
    dispatch(showChat(false))
    dispatch(selectPage(eventKey))
    dispatch(fetchRecords()) // always should fetchRecords, for new buffer
  }
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DataTablePagination)
