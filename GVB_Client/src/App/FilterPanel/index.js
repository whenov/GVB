import { connect } from 'react-redux'
import FilterPanel from './FilterPanel'

const mapStateToProps = state => ({
  filter: state.filter
})

export default connect(
  mapStateToProps
)(FilterPanel)
