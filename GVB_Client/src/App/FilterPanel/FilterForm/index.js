import { connect } from 'react-redux'
import FilterForm from './FilterForm'
import { changeFilter, resetDataTable, fetchRecords, resetRecordInfo } from '../../../actions'

const mapStateToProps = state => (
  state.filter
)

const mapDispatchToProps = (dispatch, ownProps) => ({
  onChange: e => {
    let {name, value} = e.target
    dispatch(changeFilter(name, value))
  },

  onSubmit: e => {
    e.preventDefault()

    ownProps.toggleCollapse()

    dispatch(resetRecordInfo())
    dispatch(resetDataTable())
    dispatch(fetchRecords())
  }
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FilterForm)
