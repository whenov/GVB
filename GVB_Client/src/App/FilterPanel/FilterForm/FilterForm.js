import React, { Component } from 'react'
import { Form, FormGroup, FormControl, ControlLabel, InputGroup, Radio, Button, Row, Col } from 'react-bootstrap'

const $ = require('jquery')

require('bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css')
require('bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')
require('imports?jQuery=jquery!bootstrap-datepicker/dist/locales/bootstrap-datepicker.zh-CN.min.js')

require('imports?jQuery=jquery!bootstrap-timepicker/js/bootstrap-timepicker.min.js')

class FilterForm extends Component {
  componentDidMount() {
    const datepickerConfig = {
      format: "yyyy-mm-dd",
      weekStart: 1,
      language: "zh-CN",
      autoclose: true,
      todayHighlight: true,
      clearBtn: true
    }

    $('input[name=BeginDate]').datepicker(datepickerConfig).on('changeDate', this.props.onChange)
    $('input[name=EndDate]').datepicker(datepickerConfig).on('changeDate', this.props.onChange)

    const timepickerConfig = {
      template: false, // don't show a widget
      showSeconds: true,
      showMeridian: false, // 24hr
      defaultTime: false
    }

    $('input[name=BeginTime]').timepicker(timepickerConfig).on('changeTime.timepicker', this.props.onChange)
    $('input[name=EndTime]').timepicker(timepickerConfig).on('changeTime.timepicker', this.props.onChange)
  }

  checkScore(str) {
    if (!str)
      return ""

    if (!isNaN(str)) {
      const num = parseInt(str)
      if (num >= 1 && num <= 100)
        return ""
    }

    return "has-error"
  }

  checkAllScore() {
    return (this.checkScore(this.props.VoiceScore) === "" &&
        this.checkScore(this.props.GenderScore) === "" &&
        this.checkScore(this.props.LanguageScore) === "" &&
        this.checkScore(this.props.SpeakerScore) === "" &&
        this.checkScore(this.props.KeywordScore) === "")
  }

  render() {
    const { BeginDate, BeginTime, EndDate, EndTime, Read, Voice, VoiceScore, Gender, GenderScore, Language, LanguageScore, Speaker, SpeakerScore, Keyword, KeywordScore,
      onChange, onSubmit } = this.props
    return (
      <Form horizontal onSubmit={onSubmit}>
        <FormGroup>
          <Col md={2} componentClass={ControlLabel}>
            时间段
          </Col>
          <Col md={3}>
            <FormControl type="text" name="BeginDate" value={BeginDate} placeholder="开始日期" onChange={onChange} />
          </Col>
          <Col md={2}>
            <FormControl type="text" name="BeginTime" value={BeginTime} onChange={onChange} disabled={!BeginDate} />
          </Col>
        </FormGroup>
        <FormGroup>
          <Col mdOffset={2} md={3}>
            <FormControl type="text" name="EndDate" value={EndDate} placeholder="截止日期" onChange={onChange} />
          </Col>
          <Col md={2}>
            <FormControl type="text" name="EndTime" value={EndTime} onChange={onChange} disabled={!EndDate} />
          </Col>
        </FormGroup>

        <FormGroup>
          <Col md={2} componentClass={ControlLabel}>是否已阅</Col>
          <Col md={3}>
            <Radio inline name="Read" value="" checked={Read===""} onChange={onChange}>不限</Radio>
            <Radio inline name="Read" value="0" checked={Read==="0"} onChange={onChange}>未阅</Radio>
            <Radio inline name="Read" value="1" checked={Read==="1"} onChange={onChange}>已阅</Radio>
          </Col>
        </FormGroup>

        <FormGroup>
          <Col md={2} componentClass={ControlLabel}>是否话音</Col>
          <Col md={3}>
            <Radio inline name="Voice" value="" checked={Voice===""} onChange={onChange}>不限</Radio>
            <Radio inline name="Voice" value="0" checked={Voice==="0"} onChange={onChange}>非话音</Radio>
            <Radio inline name="Voice" value="1" checked={Voice==="1"} onChange={onChange}>话音</Radio>
          </Col>
          <Col md={2} componentClass={InputGroup} className={this.checkScore(VoiceScore)}>
            <InputGroup.Addon>阈值</InputGroup.Addon>
            <FormControl type="text" name="VoiceScore" value={VoiceScore} disabled={!Voice} placeholder="0-100" onChange={onChange} />
          </Col>
        </FormGroup>

        <FormGroup>
          <Col md={2} componentClass={ControlLabel}>性别</Col>
          <Col md={3}>
            <Radio inline name="Gender" value="" checked={Gender===""} onChange={onChange}>不限</Radio>
            <Radio inline name="Gender" value="0" checked={Gender==="0"} onChange={onChange}>男</Radio>
            <Radio inline name="Gender" value="1" checked={Gender==="1"} onChange={onChange}>女</Radio>
          </Col>
          <Col md={2} componentClass={InputGroup} className={this.checkScore(GenderScore)}>
            <InputGroup.Addon>阈值</InputGroup.Addon>
            <FormControl type="text" name="GenderScore" value={GenderScore} disabled={!Gender} placeholder="0-100" onChange={onChange} />
          </Col>
        </FormGroup>

        <FormGroup>
          <Col md={2} componentClass={ControlLabel}>语种</Col>
          <Col md={3}>
            <FormControl type="text" name="Language" value={Language} placeholder="不限" onChange={onChange} />
          </Col>
          <Col md={2} componentClass={InputGroup} className={this.checkScore(LanguageScore)}>
            <InputGroup.Addon>阈值</InputGroup.Addon>
            <FormControl type="text" name="LanguageScore" value={LanguageScore} disabled={!Language} placeholder="0-100" onChange={onChange} />
          </Col>
        </FormGroup>

        <FormGroup>
          <Col md={2} componentClass={ControlLabel}>说话人</Col>
          <Col md={3}>
            <FormControl type="text" name="Speaker" value={Speaker} placeholder="不限" onChange={onChange} />
          </Col>
          <Col md={2} componentClass={InputGroup} className={this.checkScore(SpeakerScore)}>
            <InputGroup.Addon>阈值</InputGroup.Addon>
            <FormControl type="text" name="SpeakerScore" value={SpeakerScore} disabled={!Speaker} placeholder="0-100" onChange={onChange} />
          </Col>
        </FormGroup>

        <FormGroup>
          <Col md={2} componentClass={ControlLabel}>关键字</Col>
          <Col md={3}>
            <FormControl type="text" name="Keyword" value={Keyword} placeholder="不限" onChange={onChange} />
          </Col>
          <Col md={2} componentClass={InputGroup} className={this.checkScore(KeywordScore)}>
            <InputGroup.Addon>阈值</InputGroup.Addon>
            <FormControl type="text" name="KeywordScore" value={KeywordScore} disabled={!Keyword} placeholder="0-100" onChange={onChange} />
          </Col>
        </FormGroup>

        <FormGroup>
          <Col mdOffset={2} md={10}>
            <Button type="submit" bsStyle="primary" disabled={!this.checkAllScore()}>提交</Button>
          </Col>
        </FormGroup>
      </Form>
    )
  }
}

export default FilterForm
