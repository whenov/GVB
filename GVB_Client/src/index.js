"use strict"

import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { configureStore } from './store/configureStore'
import App from './App'

require('file?name=[name].[ext]!./index.html') // let webpack copy html file

const store = configureStore()

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)
