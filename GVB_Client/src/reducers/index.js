import { combineReducers } from 'redux'
import filter from './filter'
import dataTable from './dataTable'
import chat from './chat'
import player from './player'
import recordInfo from './recordInfo'

const rootReducer = combineReducers({
  filter,
  dataTable,
  chat,
  player,
  recordInfo
})

export default rootReducer
