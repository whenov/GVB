import { SELECT_PAGE, RESET_DATATABLE, REQUEST_RECORDS, RECEIVE_RECORDS, SELECT_ROW } from '../actions'

// (pageCapacity * pageBuffNum) should be smaller than maxRows of oracledb(default: 100)
const initialState = {
  pageCapacity: 10,
  activePage: 1,
  pageBuffNum: 5, // must be odd.
  records: [],
  activeRow: -1,
  isFetching: false
}

const dataTable = (state = initialState, action) => {
  if (action.type === SELECT_PAGE) {
    return Object.assign({}, state, {
      activeRow: -1,
      activePage: action.activePage
    })
  } else if (action.type === RESET_DATATABLE) {
    return Object.assign({}, state, {
      records: [],
      activeRow: -1,
      activePage: 1
    })
  } else if (action.type === RECEIVE_RECORDS) {
    return Object.assign({}, state, {
      isFetching: false,
      records: action.json
    })
  } else if (action.type === REQUEST_RECORDS) {
    return Object.assign({}, state, {
      isFetching: true
    })
  } else if (action.type === SELECT_ROW) {
    return Object.assign({}, state, {
      activeRow: action.id
    })
  } else {
    return state
  }
}

export default dataTable
