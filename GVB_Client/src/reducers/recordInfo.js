import { REQUEST_RECORD_INFO, RECEIVE_RECORD_INFO, SELECT_RECORD_INFO_TAB, RESET_RECORD_INFO, SELECT_RECORD_INFO_ROW } from '../actions'

const initialState = {
  tab: 'Language',
  infos: [],
  activeRow: -1,
  isFetching: false
}

const filter = (state = initialState, action) => {
  if (action.type === REQUEST_RECORD_INFO) {
    return Object.assign({}, state, {
      isFetching: true
    })
  } else if (action.type === RECEIVE_RECORD_INFO) {
    return Object.assign({}, state, {
      isFetching: false,
      infos: action.json
    })
  } else if (action.type === SELECT_RECORD_INFO_TAB) {
    return Object.assign({}, state, {
      tab: action.tab
    })
  } else if (action.type === RESET_RECORD_INFO) {
    return Object.assign({}, state, {
      infos: [],
      activeRow: -1,
      isFetching: false
    })
  } else if (action.type === SELECT_RECORD_INFO_ROW) {
    return Object.assign({}, state, {
      activeRow: action.id
    })
  } else {
    return state
  }
}

export default filter
