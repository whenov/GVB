import { SHOW_CHAT, REQUEST_RECORD_CONTINUOUS, RECEIVE_RECORD_CONTINUOUS, RESET_RECORD_CONTINUOUS } from '../actions'

const initialState = {
  show: false,
  speeches: [],
  isFetching: false
}

const chat = (state = initialState, action) => {
  if (action.type === SHOW_CHAT) {
    return Object.assign({}, state, {
      show: action.show
    })
  } else if (action.type === REQUEST_RECORD_CONTINUOUS) {
    return Object.assign({}, state, {
      isFetching: true
    })
  } else if (action.type === RECEIVE_RECORD_CONTINUOUS) {
    return Object.assign({}, state, {
      isFetching: false,
      speeches: action.json
    })
  } else if (action.type === RESET_RECORD_CONTINUOUS) {
    return Object.assign({}, state, {
      speeches: []
    })
  } else {
    return state
  }
}

export default chat
