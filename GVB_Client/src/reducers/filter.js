import { CHANGE_FILTER } from '../actions'

const initialState = {
  BeginDate: '',
  BeginTime: '0:00:00',
  EndDate: '',
  EndTime: '0:00:00',
  Read: '',
  Voice: '',
  VoiceScore: '',
  Gender: '',
  GenderScore: '',
  Language: '',
  LanguageScore: '',
  Speaker: '',
  SpeakerScore: '',
  Keyword: '',
  KeywordScore: ''
}

const filter = (state = initialState, action) => {
  if (action.type === CHANGE_FILTER) {
    return Object.assign({}, state, {
      [action.name]: action.value
    })
  } else {
    return state
  }
}

export default filter
