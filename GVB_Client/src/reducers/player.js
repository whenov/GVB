import { FOCUS_REGION, RESET_FOCUS_REGION, CREATE_AND_PLAY_REGION, RESET_CREATE_AND_PLAY_REGION } from '../actions'

const initialState = {
  focusRegion: false,
  regionToCreateAndPlay: null
}

const player = (state = initialState, action) => {
  if (action.type === FOCUS_REGION) {
    return Object.assign({}, state, {
      focusRegion: true
    })
  } else if (action.type === RESET_FOCUS_REGION) {
    return Object.assign({}, state, {
      focusRegion: false
    })
  } else if (action.type === CREATE_AND_PLAY_REGION) {
    return Object.assign({}, state, {
      regionToCreateAndPlay: action.region
    })
  } else if (action.type === RESET_CREATE_AND_PLAY_REGION) {
    return Object.assign({}, state, {
      regionToCreateAndPlay: null
    })
  } else {
    return state
  }
}

export default player
